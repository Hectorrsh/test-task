package entities;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class Translate {
    @SerializedName("translations")
    public List<Translations> translations;
}
