package gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import entities.Translate;
import java.util.logging.Logger;

public class YandexTranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "Bearer t1.9euelZqKkc7Jk5uPicial5SRnpyZnu3rnpWancnKj8mUzZLKiZWYisqUyZjl8_c3H2h2-e8lYlQS_d3z93dNZXb57yViVBL9.7PLbIfUYtAo0r6uyVQcEDoHJ9jW5FJTaaXBocpxL3jACaFCK67BCJ1trk-KS7yCuqOVU3B7kSSKvk7IdScjmDw";
    private static final String FOLDER_ID = "b1gffho63ohepfp2l491";

    @SneakyThrows
    public Translate getTranslate(String texts, String targetLanguageCode) {
        Gson gson = new Gson();

        HttpResponse<String> response = Unirest.post(URL)
                .header("Content-Type", "application/json")
                .header("Authorization", TOKEN)
                .queryString("folderId", FOLDER_ID)
                .queryString("texts", texts)
                .queryString("targetLanguageCode", targetLanguageCode)
                .asString();
        String strResponse = response.getBody();
        Logger log = Logger.getLogger(YandexTranslateGateway.class.getName());
        log.info(strResponse);
        return gson.fromJson(strResponse, Translate.class);
    }
}