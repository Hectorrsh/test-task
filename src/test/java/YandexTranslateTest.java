import org.junit.jupiter.api.DisplayName;
import entities.Translate;
import entities.Translations;
import gateway.YandexTranslateGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class YandexTranslateTest {

    @Test
    @DisplayName("Перевод фразы \"Hello World!\" на русский язык")
    public void getSomeTranslate(){
        String translatableText = "Hello World!";
        String targetLanguageCode = "ru";
        String targetText = "Привет Мир!";
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        Translate someTranslation = yandexTranslateGateway.getTranslate(translatableText, targetLanguageCode);
        StringBuilder translatedText = new StringBuilder();
        for (Translations translate : someTranslation.translations) {
            translatedText.append(translate.text.replace("+", " ").replaceAll("[\\s]{2,}", " "));
        }
        Assertions.assertEquals(translatedText.toString().trim(), targetText);
    }
}
